import App from './app'

if (process.env.NODE_ENVIROMENT_TYPE !== 'production') {
    require('dotenv').config();
}

App.express.listen(process.env.PORT || 3333, () => {
    console.log("proccess started")
})