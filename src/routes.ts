import { Router } from 'express';
import ProductController from './controllers/product/ProductController';

const routes = Router()

routes.get('/product', ProductController.index)
routes.get('/product/:id', ProductController.show)
routes.put('/product/:id', ProductController.update)
routes.post('/product', ProductController.create)
routes.delete('/product/:id', ProductController.destroy)

export default routes;