import { Schema, model, Document } from 'mongoose';

interface ProductInterface extends Document {
    titulo?: string;
    descricao?: string
    url?: string
}

const ProductSchema = new Schema({

    titulo: String,
    descricao: String,
    url: String,
})


export default model<ProductInterface>('Product', ProductSchema)