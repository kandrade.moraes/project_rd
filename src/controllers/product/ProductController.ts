import mongoose from 'mongoose';
import Product from '../../models/product/Product'
import { Request, Response } from 'express'


class ProductController {

    public async index(req: Request, res: Response): Promise<Response> {
        const PRODUCTS = await Product.find()
        return res.status(200).json(PRODUCTS)
    }

    public async show(req: Request, res: Response): Promise<Response> {
        const PRODUCTS = await Product.findById(req.params.id)
        return res.status(200).json(PRODUCTS)
    }

    public async update(req: Request, res: Response): Promise<Response> {
        const PRODUCTS = await Product.findByIdAndUpdate(req.params.id, req.body, { new: true })
        return res.status(200).json(PRODUCTS)
    }

    public async create(req: Request, res: Response): Promise<Response> {
        const PRODUCT = await Product.create(req.body)
        return res.status(200).json(PRODUCT)
    }
    
    public async destroy(req: Request, res: Response): Promise<Response> {
        const PRODUCT = await Product.findByIdAndDelete(req.params.id)
        return res.status(204).json(PRODUCT)
    }
}

export default new ProductController()