import * as dotenv from "dotenv";

class MongoConfig {

  constructor(
    private URL = process.env.MONGO_URL, 
    private CONFIG = {
    useUnifiedTopology: true,
    useFindAndModify: true,
    useNewUrlParser: true,
    useCreateIndex: true,
  }) {

  }

  get MONGO_URL() : string {
    return this.MONGO_URL
  }

  get MONGO_CONFIG() : {} {
    return this.MONGO_CONFIG
  }
}

export default new MongoConfig;